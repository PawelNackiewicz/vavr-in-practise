package option;

import io.vavr.control.Option;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Optional;

@Slf4j
public class OptionTest {

    private static final Logger logger = LoggerFactory.getLogger(OptionTest.class);

    @Test
    void optionTest() {
        Optional<String> stringOptional = Optional.of("value");
        Optional<String> optional = stringOptional.map(s -> (String) null).map(s -> s.toUpperCase());

        logger.info("optional " + optional);

        Option<String> stringOption = Option.of("vavrValue").map(s -> s.toUpperCase());

        logger.info("varv-result" + stringOption);
    }

}
