package tryExample;

import io.vavr.control.Try;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Slf4j
public class TryTest {

    private static final Logger logger = LoggerFactory.getLogger(TryTest.class);

    @Test
    void earlier() {
        try {
            calculate();
        } catch (FirstException e) {
            e.printStackTrace();
        } catch (OtherException e) {
            e.printStackTrace();
        }
    }

    @Test
    void withVavr() {
        Try<Integer> result = Try.of(this::calculate);
        Try<Integer> recover = result.recover(FirstException.class, 10)
                .recover(OtherException.class, 20);
        logger.info("result: " + recover);

        Integer orElse = result.getOrElse(50);
        logger.info("Get or else: " + orElse);

        Integer recoverOrElse = recover.getOrElse(50);
        logger.info("Recovered get or else: " + recoverOrElse);
    }

    private Integer calculate() throws FirstException, OtherException {
        if (1==1) {
            throw new FirstException();
        }
        if(1==3) {
            throw new OtherException();
        }
        return 42;
    }

}
